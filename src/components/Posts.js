import React, {Component} from 'react';
import Post from './Post';
import User from './User';

export default class Posts extends Component {
    render() {
        return(
            <div className="left">
                <Post alt="nature" src="https://images.pexels.com/photos/459225/pexels-photo-459225.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"/>
                <Post alt="nature" src="https://images.pexels.com/photos/459225/pexels-photo-459225.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"/>
            </div>
        )
    }
}